package br.com.itau.numeros.romanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AssertionsKt;
import org.junit.jupiter.api.Test;

public class ConversorTest {
    private String romano;

    @Test
    public void testaConversao(){
        this.romano = "LXXXIX";
        Assertions.assertEquals(89, Conversor.traduzirNumeralRomano(this.romano));
    }

    @Test
    public void testaConversaoNegativo(){
        this.romano = "-LXXXIX";
        Assertions.assertEquals(89, Conversor.traduzirNumeralRomano(this.romano));
    }
}
